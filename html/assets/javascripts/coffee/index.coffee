module.exports = [
  # jQuery
  "jquery-2.1.0.min"
  "jquery-migrate-1.2.1.min"
  "bootstrap.min"

  # "jquery.browser"

  # NAVIGATION
  # "jquery.headshrinker"                         # Fixed navigation header
  # "jquery.scrollup"                             # Scroll to top arrow
  # "jquery.slicknav"                             # Responsive navigation
  "jquery.bigslide.min"                           # Side slide in navigation
  "jquery.dropit"                                 # Dropdown menu
  


  # UTILITIES
  # "jquery.lazy"                                 # Delays the loading of images only when they come into view
  "jquery.simplemodal"
  "jquery.slider"
  # "jquery.loading"                              # Loading screen to hide partially rendered page
  "jquery.tabs"
  "rrssb"

  # IMAGES
  "jquery.slick"                              # Images carousel

  # TEXT MANIPULATION
  "jquery.dotdotdot"


  # APPLICATION
  "application"

  # owl carousel
  "owl.carousel" 
]
