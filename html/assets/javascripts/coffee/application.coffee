$(document).ready ->
      
  $('#search').on 'click', (e) ->
    $container = undefined
    e.preventDefault()
    $container = $(this).parents('.header__heading').find('.header__search').fadeIn(100)
    $container.fadeIn()
    $container.find('.header__search-text').focus()
  $('.header__search-close').on 'click', (e) ->
    e.preventDefault()
    $(this).parents('.header__search').hide()
  $('#login').on 'click', (e) ->
    e.preventDefault()
    $('.login-form').simplemodal
      overlayClose: true
      opacity: 80
      fixed: false
      zIndex: 11010
  
  
  $('#profile').on 'click', (e) ->
      $('.header__heading-container').toggleClass 'Profile_Open'
      $('body').toggleClass 'no_profile'
      e.preventDefault()
      return
      
  $('.profile_overlay').on 'click', (e) ->
      $('.header__heading-container').removeClass 'Profile_Open'
      $('body').removeClass 'no_profile'
      e.preventDefault()
      return
  
  
  $('.tabs').tabs()

  $('.calendar-list-day').on 'click', (e) ->
    $events = undefined
    $item = undefined
    e.preventDefault()
    $item = $(this).parent('.calendar-list-item')
    $events = $(this).siblings('.calendar-list-event')
    if $item.hasClass('calendar-list-item--expand')
      $events.slideUp 100, ->
        $item.removeClass 'calendar-list-item--expand'
    else
      $events.slideDown 100, ->
        $item.addClass 'calendar-list-item--expand'
  $('.calendar__event').on 'click', (e) ->
    e.preventDefault()
    $('.calendar-details').simplemodal
      appendTo: '.calendar-details--modal'
      overlayClose: true
      containerId: 'calendar-details--container'
      overlayId: 'calendar-details--overlay'
      closeClass: 'calendar-details__close'
      fixed: false
      opacity: 60
      zIndex: 11000
  $('.slides').slick
    prevArrow: '<a href="#" class="slick-prev">Prev</a>'
    nextArrow: '<a href="#" class="slick-next">Next</a>'
    speed: 400
    autoplay: true
    autoplaySpeed: 3000
    draggable: false
    slidesToShow: 1
    dots: true
    customPaging: (slider, i) ->
      img_src = undefined
      img_src = $(slider.$slides[i]).data('thumbnail')
      '<button type=\'button\' data-role=\'none\' style=\'background-image: url(' + img_src + ')\'></button>'


$(document).ready ->

    #! Navigation
    $navigation = '.responsive-navigation'
    $nextItem = '<span class="next-item"><i class="fa fa-angle-right"></i></span>'
    $back = '<li class="back"><a href="javascript:;"><i class="fa fa-chevron-left"></i>Back</a></li>'
    
    
    $(".responsive-navigation .responsive-navigation__list li:has(ul)").prepend $nextItem
    $(".responsive-navigation .responsive-navigation__list .sub-menu").prepend $back
    
    $('#header-responsive').on 'click', ->
        $('body').addClass('no-scroll');
        $('.responsive-navigation').addClass 'navigation-active'
        return
    
    $('.responsive-navigation .next-item').on 'click', (e) ->
        e.preventDefault()
        $(this).nextAll('.sub-menu').addClass 'navigation-active'
        return
      
    $('.responsive-navigation .back').on 'click', (e) ->
        e.preventDefault()
        $(this).parent('.sub-menu').removeClass 'navigation-active'
        return
      
    $('.close-menu').on 'click', ->
        $('.responsive-navigation').removeClass 'navigation-active'
        $('body').removeClass('no-scroll');
        return
    
    $('.menu-overlay').on 'click', ->
        $('.responsive-navigation').removeClass 'navigation-active'
        $('body').removeClass('no-scroll');
        return
    
    $(".header__navigation-list li:has(ul)").addClass("menu-item-has-children")
    $(".header__navigation-list li:has(ul) > a").addClass("has-child")
    return

  cardHolder = ''
  $(window).load ->
    clearTimeout cardHolder
    cardHolder = setTimeout((->
      $('.card p, .card h1').dotdotdot({watch: true})
      return
    ), 750)
    return
