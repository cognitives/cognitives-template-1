(($) ->
  "use strict"

  $.fn.slider = (method) ->
    methods =
      init: (options)->
        @.slider.settings = $.extend(@.slider.defaults, options)
        @.each ->
          $(@).on "click", (e) ->
            e.preventDefault()

            settings = $.extend($.fn.slider.settings, options)

            $link      = $(@)
            $container = settings.container

            if $link.hasClass(settings.expandClass)
              $container.slideUp 100, ->
                $link.removeClass(settings.expandClass)
            else
              $container.slideDown 100, ->
                $link.addClass(settings.expandClass)

    methods.init.apply(@, arguments)

  # Default settings
  $.fn.slider.defaults =
    container:   $(".contents")
    expandClass: "expand"

  $.fn.slider.settings = {}

) jQuery
