var cardHolder;

$(document).ready(function() {
  $('#search').on('click', function(e) {
    var $container;
    $container = void 0;
    e.preventDefault();
    $container = $(this).parents('.header__heading').find('.header__search').fadeIn(100);
    $container.fadeIn();
    return $container.find('.header__search-text').focus();
  });
  $('.header__search-close').on('click', function(e) {
    e.preventDefault();
    return $(this).parents('.header__search').hide();
  });
  $('#login').on('click', function(e) {
    e.preventDefault();
    return $('.login-form').simplemodal({
      overlayClose: true,
      opacity: 80,
      fixed: false,
      zIndex: 11010
    });
  });
  $('#profile').on('click', function(e) {
    $('.header__heading-container').toggleClass('Profile_Open');
    $('body').toggleClass('no_profile');
    e.preventDefault();
  });
  $('.profile_overlay').on('click', function(e) {
    $('.header__heading-container').removeClass('Profile_Open');
    $('body').removeClass('no_profile');
    e.preventDefault();
  });
  $('.tabs').tabs();
  $('.calendar-list-day').on('click', function(e) {
    var $events, $item;
    $events = void 0;
    $item = void 0;
    e.preventDefault();
    $item = $(this).parent('.calendar-list-item');
    $events = $(this).siblings('.calendar-list-event');
    if ($item.hasClass('calendar-list-item--expand')) {
      return $events.slideUp(100, function() {
        return $item.removeClass('calendar-list-item--expand');
      });
    } else {
      return $events.slideDown(100, function() {
        return $item.addClass('calendar-list-item--expand');
      });
    }
  });
  $('.calendar__event').on('click', function(e) {
    e.preventDefault();
    return $('.calendar-details').simplemodal({
      appendTo: '.calendar-details--modal',
      overlayClose: true,
      containerId: 'calendar-details--container',
      overlayId: 'calendar-details--overlay',
      closeClass: 'calendar-details__close',
      fixed: false,
      opacity: 60,
      zIndex: 11000
    });
  });
  return $('.slides').slick({
    prevArrow: '<a href="#" class="slick-prev">Prev</a>',
    nextArrow: '<a href="#" class="slick-next">Next</a>',
    speed: 400,
    autoplay: true,
    autoplaySpeed: 3000,
    draggable: false,
    slidesToShow: 1,
    dots: true,
    customPaging: function(slider, i) {
      var img_src;
      img_src = void 0;
      img_src = $(slider.$slides[i]).data('thumbnail');
      return '<button type=\'button\' data-role=\'none\' style=\'background-image: url(' + img_src + ')\'></button>';
    }
  });
});

$(document).ready(function() {
  var $back, $navigation, $nextItem;
  $navigation = '.responsive-navigation';
  $nextItem = '<span class="next-item"><i class="fa fa-angle-right"></i></span>';
  $back = '<li class="back"><a href="javascript:;"><i class="fa fa-chevron-left"></i>Back</a></li>';
  $(".responsive-navigation .responsive-navigation__list li:has(ul)").prepend($nextItem);
  $(".responsive-navigation .responsive-navigation__list .sub-menu").prepend($back);
  $('#header-responsive').on('click', function() {
    $('body').addClass('no-scroll');
    $('.responsive-navigation').addClass('navigation-active');
  });
  $('.responsive-navigation .next-item').on('click', function(e) {
    e.preventDefault();
    $(this).nextAll('.sub-menu').addClass('navigation-active');
  });
  $('.responsive-navigation .back').on('click', function(e) {
    e.preventDefault();
    $(this).parent('.sub-menu').removeClass('navigation-active');
  });
  $('.close-menu').on('click', function() {
    $('.responsive-navigation').removeClass('navigation-active');
    $('body').removeClass('no-scroll');
  });
  $('.menu-overlay').on('click', function() {
    $('.responsive-navigation').removeClass('navigation-active');
    $('body').removeClass('no-scroll');
  });
  $(".header__navigation-list li:has(ul)").addClass("menu-item-has-children");
  $(".header__navigation-list li:has(ul) > a").addClass("has-child");
});

cardHolder = '';

$(window).load(function() {
  clearTimeout(cardHolder);
  cardHolder = setTimeout((function() {
    $('.card p, .card h1').dotdotdot({
      watch: true
    });
  }), 750);
});
