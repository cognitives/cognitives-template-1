$(document).ready(function() {
    $(".header__heading-link--responsive").bigSlide({
        menu: ".side-navigation",
        menuWidth: $(window).width() < 480 ? "260px" : "400px",
        easyClose: true,
        side: "right",
        activeBtn: "close",
        beforeOpen: function() {
            return $(this.menu).css("visibility", "visible");
        },
        afterClose: function() {
            return $(this.menu).css("visibility", "hidden");
        }
    });
    $("#search").on("click", function(e) {
        var $container;
        e.preventDefault();
        $container = $(this).parents(".header__heading").find(".header__search").fadeIn(100);
        $container.fadeIn();
        return $container.find(".header__search-text").focus();
    });
    $(".header__search-close").on("click", function(e) {
        e.preventDefault();
        return $(this).parents(".header__search").hide();
    });
    $("#login").on("click", function(e) {
        e.preventDefault();
        return $(".login-form").modal({
            overlayClose: true,
            opacity: 80,
            fixed: false,
            zIndex: 11010
        });
    });
    $("#profile").on("click", function(e) {
        e.preventDefault();
        return $("#header__menu").modal({
            modal: true,
            overlayClose: true,
            opacity: 0.9,
            fixed: false,
            zIndex: 11010,
            closeHTML: "",
            autoPosition: false,
            onShow: function(object) {
                var header;
                header = $("header").height();
                return $(object.container).css("top", header + "px");
            }
        });
    });
    $(".tabs").tabs();
    $("#map").lazyLoadGoogleMaps({
        callback: function(container, map) {
            var $center, $container, $map;
            $container = $(container);
            $map = map;
            $center = new google.maps.LatLng($container.attr("data-lat"), $container.attr("data-lng"));
            $map.setOptions({
                zoom: 15,
                center: $center
            });
            return new google.maps.Marker({
                position: $center,
                map: $map
            });
        }
    });
    $(".calendar-list-day").on("click", function(e) {
        var $events, $item;
        e.preventDefault();
        $item = $(this).parent(".calendar-list-item");
        $events = $(this).siblings(".calendar-list-event");
        if ($item.hasClass("calendar-list-item--expand")) {
            return $events.slideUp(100, function() {
                return $item.removeClass("calendar-list-item--expand");
            });
        } else {
            return $events.slideDown(100, function() {
                return $item.addClass("calendar-list-item--expand");
            });
        }
    });
    $(".calendar__event").on("click", function(e) {
        e.preventDefault();
        return $(".calendar-details").modal({
            appendTo: ".calendar-details--modal",
            overlayClose: true,
            containerId: "calendar-details--container",
            overlayId: "calendar-details--overlay",
            closeClass: "calendar-details__close",
            fixed: false,
            opacity: 60,
            zIndex: 11000
        });
    });
    $(".slides").slick({
        prevArrow: '<a href="#" class="slick-prev">Prev</a>',
        nextArrow: '<a href="#" class="slick-next">Next</a>',
        speed: 400,
        autoplay: true,
        autoplaySpeed: 3000,
        draggable: false,
        slidesToShow: 1,
        dots: true,
        customPaging: function(slider, i) {
            var img_src;
            img_src = $(slider.$slides[i]).data("thumbnail");
            return "<button type='button' data-role='none' style='background-image: url(" + img_src + ")'></button>";
        }
    });


	/***********************************/
	/* Stop Video with Modal Close */
	/**********************************/
	$('#videoModal').on('hide.bs.modal', function(e) {
		var $if = $(e.delegateTarget).find('iframe');
		var src = $if.attr("src");
		$if.attr("src", '/empty.html');
		$if.attr("src", src);
	});
	
	// Fill modal with content from link href
	$("#AjaxModal").on("show.bs.modal", function(e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-body").load(link.attr("href"));
	});
	
	$("#AjaxUserProfileModal").on("show.bs.modal", function(e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-body").load(link.attr("href"));
	});
	
	$("#AjaxMyPlatformModal").on("show.bs.modal", function(e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-body").load(link.attr("href"));
	});
	
	
	
    return $(".card p, .card h1").dotdotdot();

});