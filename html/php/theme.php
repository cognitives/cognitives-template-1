<?php require "partials/_header.php"; ?>

<div class="section section--local">
    <div class="section__content">
        <div class="row">
            <div class="col-full"><?php require "partials/cards/_news-01.php"; ?></div>
        </div>
    </div>
</div>

<div class="section section--local">
    <div class="section__content">
        <div class="row">
            <div class="col-third"><?php require 'partials/cards/_news-02.php'; ?></div>
            <div class="col-third"><?php require "partials/cards/_twitter.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_news-02.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_facebook.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_youtube.php"; ?></div>
            <div class="clear"></div>
            <div class="col-quarter"><?php require "partials/cards/_news-02.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_twitter.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_youtube.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_facebook.php"; ?></div>
        </div>
    </div>
</div>

<div class="section">
    <div class="section__content">
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_news-02.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_twitter.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_youtube.php"; ?></div>
            <div class="col-third"><?php require "partials/cards/_news-02.php"; ?></div>
            <div class="col-third"><?php require "partials/cards/_twitter.php"; ?></div>
            <div class="col-third"><?php require "partials/cards/_youtube.php"; ?></div>
        </div>
    </div>
</div>


<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<?php require "partials/_footer.php"; ?>