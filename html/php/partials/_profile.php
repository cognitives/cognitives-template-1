<div id="header__menu">
    <div class="profile__menu">
        <div class="profile__menu-image" style="background-image: url('http://res.cloudinary.com/cognitives/image/upload/c_thumb,dpr_auto,g_face,h_100,r_max,w_100/yniifzs0temoqvglxv29');"></div>
        <div class="profile__menu-content">
            <div class="profile__menu-name">Jamie Stein</div>
            <div class="profile__menu-email">jamie.s@gmail.com</div>
            <a href="#" class="profile__menu-button">My News</a>
        </div>
        <ul class="profile__menu-navigation">
            <li><a class="profile__menu-navigation-link" href="login.php">Login</a></li>
            <li><a class="profile__menu-navigation-link" href="#">View Profile</a></li>
            <li><a class="profile__menu-navigation-link" href="#">Settings</a></li>
            <li><a class="profile__menu-navigation-link" href="#">Sign Out</a></li>
        </ul>
    </div>
</div>
