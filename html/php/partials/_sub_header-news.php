<div class="sub_header">
    <div class="sub_header__container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="sub_header_section">
                    <div class="sub_header--left">
                        <a href="/" class="sub_header__heading">
                            Category 1
                        </a>
                        <div class="sub_header__breadcrumb">
                            <a href="/" class="sub_header__breadcrumb-link">Category 1</a>
                            <a href="/" class="sub_header__breadcrumb-link">Article</a>
                        </div>
                    </div>
                    <div class="sub_header--right">
                        <a href="/" class="sub_header__button">Follow +</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
