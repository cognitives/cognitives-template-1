<div class="sub_header blog_sub_header">
  <div class="sub_header__container">
      <div class="row">
          <div class="col-md-12 col-xs-12">
              <div class="sub_header_section">
                  <div class="sub_header--left">
                      <a href="/" class="sub_header__heading">
                        Blog<strong>Blog Title</strong>
                      </a>
                      <div class="sub_header__breadcrumb"> by <a href="javascript:;">USER NAME</a> </div>
                  </div>
                  <div class="sub_header--right">
                    <a href="/" class="sub_header__button">Follow +</a>
                  </div>

              </div>
          </div>
      </div>
  </div>
</div>
