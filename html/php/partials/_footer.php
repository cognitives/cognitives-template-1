  <footer class="footer">
    <div class="footer__container">
      <div class="footer__info">
        <a href="/" class="footer__logo"><img src="static/images/themeLogo_Footer.svg" class="img-responsive" alt="Logo"></a>
        <div class="footer__copyright">Copyright 2016</div>
      </div>
      <div class="footer__navigation">
        <div class="footer__navigation--col-half">
          <ul class="footer__navigation--col-1">
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 1</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 2</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 3</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 4</a></li>
          </ul>
          <ul class="footer__navigation--col-2">
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 5</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 6</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 7</a></li>
          </ul>
        </div>
        <div class="footer__navigation--col-half">
          <ul class="footer__navigation--col-3">
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Contact us</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">About Us</a></li>
            <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Terms & Conditions</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>