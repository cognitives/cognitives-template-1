<a href="#" class="card card__youtube withImage__content">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__youtube-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>

    <div class="withImage_card_content social">
        <div class="card__youtube-category title-section">Youtube</div>
        <div class="card__youtube-icon"></div>

        <p class="card__news-description truncate">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>

        <div class="card__youtube-caption">
            <div class="card__youtube-author author_name">Social Name</div>
            <div class="card__youtube-date post_date">Oct 5 2015</div>
        </div>
    </div>
</a>
