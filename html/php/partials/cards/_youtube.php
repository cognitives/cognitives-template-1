<a href="#" class="card card__youtube">
    <div class="card__youtube-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <div class="card__youtube-category">Youtube</div>
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__youtube-icon"></div>
    <h1 class="card__youtube-heading">Video headline which runs over two lines </h1>
    <p class="card__youtube-description">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
    <div class="card__youtube-caption">
        <div class="card__youtube-author">Social Name</div>
        <div class="card__youtube-date">Oct 5 2015</div>
    </div>
</a>
