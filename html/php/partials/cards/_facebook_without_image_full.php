<a href="#" class="card card__facebook without__image">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__facebook-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    
    <div class="withImage_card_content social">
        <div class="card__facebook-category">Facebook</div>
        <div class="card__facebook-icon"></div>
        
        <p class="card__news-description truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <div class="card__facebook-caption">
            <div class="card__facebook-author">Social Name</div>
            <div class="card__facebook-date">Oct 5 2015</div>
        </div>
    </div>
</a>