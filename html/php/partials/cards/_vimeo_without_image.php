<a href="#" class="card card__vimeo">
    <div class="card__vimeo-icon-no-image"></div>
    <div class="card__vimeo-category">Vimeo</div>
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__vimeo-icon"></div>
    <p class="card__vimeo-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sedsit amet, consectetur adipisicing elit, sedsit amet, consectetur adipisicing elit, sed do #eiusmod tempor...</p>
    <div class="card__vimeo-caption">
        <div class="card__vimeo-author">Social Name</div>
        <div class="card__vimeo">Oct 5 2015</div>
    </div>
</a>
