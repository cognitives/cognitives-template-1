<a href="#" class="card card__facebook without__image">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__facebook-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <div class="content_section">
        <div class="card__facebook-category title-section">Facebook</div>
        <div class="card__facebook-icon"></div>

        <p class="card__facebook-description description">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat... Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
        <div class="card__facebook-caption caption_bottom">
            <div class="card__facebook-author author_name">Social Name</div>
            <div class="card__facebook-date post_date">Oct 5 2015</div>
        </div>
    </div>
</a>
