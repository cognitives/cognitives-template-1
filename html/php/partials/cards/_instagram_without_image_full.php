<a href="#" class="card card__instagram without__image">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__instagram-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    
    <div class="withImage_card_content social">
        <div class="card__instagram-category">Instagram</div>
        <div class="card__instagram-icon"></div>
        
        <p class="card__news-description truncate">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
        
        <div class="card__instagram-caption">
            <div class="card__instagram-author">Social Name</div>
            <div class="card__instagram-date">Oct 5 2015</div>
        </div>
    </div>
</a>