<a href="#" class="card card__twitter withImage__content">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__twitter-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <div class="withImage_card_content">
        <div class="card__twitter-category title-section">Twitter</div>
        <div class="card__twitter-icon"></div>

        <p class="card__twitter-description description">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat... Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
        <div class="card__twitter-caption caption_bottom">
            <div class="card__twitter-author author_name">Social Name</div>
            <div class="card__twitter-date post_date">Oct 5 2015</div>
        </div>
    </div>
</a>
