<a href="#" class="card card__facebook">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card_content">
        <div class="card__facebook-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
        <div class="card__facebook-category">Facebook</div>
        <div class="card__facebook-icon"></div>
        <p class="card__facebook-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sedsit amet, consectetur adipisicing elit, sedsit amet, consectetur adipisicing elit, sed do #eiusmod tempor...</p>
        <div class="card__facebook-caption">
            <div class="card__facebook-author">Social Name</div>
            <div class="card__facebook-date">Oct 5 2015</div>
        </div>
    </div>
</a>
