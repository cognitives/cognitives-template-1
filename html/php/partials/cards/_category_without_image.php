<a href="#" class="card card__news without__image">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__news-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <div class="content_section">
        <div class="card__news-category title-section">Category</div>
        <div class="card__news-icon"></div>
        <h1 class="card__news-heading">Article headline which runs over two lines </h1>
        <p class="card__news-description description">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat... Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
        <div class="card__news-caption caption_bottom">
            <div class="card__news-author author_name">Social Name</div>
            <div class="card__news-date post_date">Oct 5 2015</div>
        </div>
    </div>
</a>
