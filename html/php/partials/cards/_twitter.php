<a href="#" class="card card__twitter">
    <div class="card__twitter-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__twitter-category">Twitter</div>
    <div class="card__twitter-icon"></div>
    <p class="card__twitter-description">Lorem ipsum dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse sit amet #eiusmod tempor...</p>

    <div class="card__twitter-caption">
        <div class="card__twitter-author">@socialuser</div>
        <div class="card__twitter-date">Oct 5 2015</div>
    </div>
</a>
