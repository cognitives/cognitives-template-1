<a href="#" class="card card__instagram">
    <div class="card__instagram-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <p class="card__instagram-category">Instagram</p>
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__instagram-icon"></div>
    <p class="card__instagram-tags">#eiusmod #tempor #dolor tempor incididunt ut dolor sit amet, consectetur do eiusmod tempor incididunt ut...</p>
    <div class="card__instagram-caption">
        <div class="card__instagram-author">@socialuser</div>
        <div class="card__instagram-date">Oct 5 2015</div>
    </div>
</a>
