<a href="#" class="card card__feature">
    <img class="card__feature-image" src="images/feature.png">
    <h1 class="card__feature-heading">Blog title to go here</h1>
    <div class="card__feature-username">By User Name</div>
    <div class="card__feature-button">Follow +</div>
</a>