<a href="#" class="card no-image card__news card--rural">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card_content">
        <div class="card__news-image" style="background-image:url()"></div>
        <div class="card__news-category">Category</div>
        <div class="card__news-icon"></div>
        <h1 class="card__news-heading truncate">Article headline which runs over three lines</h1>
        <p class="card__news-description truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <div class="card__news-caption">
            <span class="card__news-by">BY</span>
            <div class="card__news-author">Tina Arena</div>
            <div class="card__news-date">Oct 5 2015</div>
            <div class="card__news-icon-share st_sharethis_large" displayText="ShareThis"></div>
        </div>
    </div>
</a>