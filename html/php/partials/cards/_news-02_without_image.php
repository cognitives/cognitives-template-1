<a href="#" class="card no-image card__news card--local without__image">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card_content">
        <div class="card__news-image-no-image"></div>
        <div class="card__news-category">Category</div>
        <div class="card__news-icon"></div>
        <h1 class="card__news-heading">Article headline which runs over two lines </h1>
        <p class="card__news-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolor sit amet, consectetur sed do eiusmod tempor incididunt ut dolor sit amet, consectetur do eiusmod tempor incididunt ut...</p>
        <div class="card__news-caption">
            <span class="card__news-by">BY</span>
            <div class="card__news-author">Rob Steel</div>
            <div class="card__news-date">Oct 5 2015</div>
            <div class="card__news-icon-share st_sharethis_large" displayText="ShareThis"></div>
        </div>
    </div>
</a>