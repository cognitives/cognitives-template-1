<a href="#" class="card card__vimeo only__image">
    <?php require 'partials/cards/_btn-overlay.php'; ?>
    <div class="card__vimeo-image" style="background-image:url('http://theganeshaexperience.com/wp-content/uploads/2012/08/Lord-Ganesha.jpg')"></div>
    <div class="content_section">
        <div class="card__vimeo-category title-section">Vimeo</div>
        <div class="card__vimeo-icon"></div>

        <p class="card__vimeo-description description">Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat... Lorem ipsum dolor sit amet, consec tetur  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
        <div class="card__vimeo-caption caption_bottom">
            <div class="card__vimeo-author author_name">Social Name</div>
            <div class="card__vimeo-date post_date">Oct 5 2015</div>
        </div>
    </div>
</a>
