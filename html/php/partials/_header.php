<?php require "functions/functions.php"; ?>

<!DOCTYPE html>
<!--[if lte IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if gt IE 8]>
<html class="ie" lang="en-US">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="UTF-8" />
    <title>Local News</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta property="fb:app_id" content="" />
    <meta property="og:title" content="Cognitives Live" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="images/opengraph-logo.png" />
    <meta property="og:site_name" content="Cognitives Live" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:image" content="images/twitter-card.png" />
    <meta name="twitter:site" content="" />
    <meta name="twitter:creator" content="" />
    <meta property="twitter:image:width" content="1200" />
    <meta property="twitter:image:height" content="630" />

    <!-- Country News Stylesheet -->
    <!-- <link rel="stylesheet" href="http://ww2.countrynews.com.au/resources/countrynews/resources/deploy/all.min.css"> -->

    <!-- Country News Javascript -->
    <!-- <script type="text/javascript" src="http://ww2.countrynews.com.au/resources/countrynews/resources/deploy/plugins.min.js?time=2015121501"></script>
    <script type="text/javascript" src="http://ww2.countrynews.com.au/resources/countrynews/resources/deploy/app.min.js?time=2015121501"></script>
    -->

    <!--[if lte IE 8]><script type="text/javascript" src="ie.js"></script><![endif]-->

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="static/css/index.css" type="text/css" media="all">

    <script type="text/javascript" src="static/js/index.js"></script>
    <!-- <script type="text/javascript" src="static/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="static/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="static/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="static/js/jquery.bigslide.min.js"></script>
    <script type="text/javascript" src="static/js/jquery.dotdotdot.js"></script>
    <script type="text/javascript" src="static/js/jquery.dropit.js"></script>
    <script type="text/javascript" src="static/js/jquery.lazy-load-google-maps.js"></script>
    <script type="text/javascript" src="static/js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="static/js/jquery.slick.min.js"></script>
    <script type="text/javascript" src="static/js/jquery.slider.js"></script>
    <script type="text/javascript" src="static/js/jquery.tabs.js"></script>
    <script type="text/javascript" src="static/js/owl.carousel.js"></script>
    <script type="text/javascript" src="static/js/application.js"></script> -->

    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "35b0a218-1b4c-491b-905f-33d25b1d73ea", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>

<body class="body">
<header class="header">
    <div class="header__heading">
        <a href="javascript:;" id="header-responsive" class="header__heading-link header__heading-link--responsive"></a>
        <div class="header__heading-container">
            <div class="header__heading--left">
                <a href="index.php" class="header__heading-logo">
                    <img src="static/images/themeLogo.svg" alt="Logo">
                </a>
            </div>
            <div class="header__heading--right">
            <!-- <a href="#" class="header__heading-link" id="login">Sign In</a> -->
            </div>
            <a href="javascript:;" class="header__heading-link header__heading-link--profile user__login" id="profile"></a>
            <?php require "partials/_profile.php" ?>
        </div>
    </div>
    <?php require "partials/_header-navigation.php"; ?>
    <?php require "partials/_side-navigation.php"; ?>
</header>

<div class="profile_overlay"></div>
