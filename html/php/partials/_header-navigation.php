<div class="header__navigation-responsive">
Menu
</div>
<div class="header__navigation">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="header__navigation-row">
                    <ul class="header__navigation-list">
                        <li class="header__navigation-item"><a href="home.php" class="header__navigation-link header__navigation-selected">Home</a></li>
                        <li class="header__navigation-item"><a href="theme.php" class="header__navigation-link">News</a>
                            <ul class="sub-menu">
                                <li><a href="#">Lorem Ipsum</a></li>
                                <li><a href="#">Lorem Ipsum Dolor</a></li>
                                <li><a href="#">Lorem Ipsum Sit</a></li>
                                <li><a href="#">Lorem Ipsum Amet</a></li>
                            </ul>
                        </li>
                        <li class="header__navigation-item"><a href="blog.php" class="header__navigation-link external-link" target="_blank">Blog</a></li>
                        <li class="header__navigation-item"><a href="partials/_login-form.php" data-toggle="modal" data-target="#AjaxMyPlatformModal" data-remote="false" class="header__navigation-link">Property</a></li>
                    </ul>
                    <?php require "partials/_search.php" ?>
                </div>
            </div>
        </div>
    </div>
</div>
