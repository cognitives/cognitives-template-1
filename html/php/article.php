<?php require "partials/_header.php"; ?>
<?php require "partials/_sub_header-news.php"; ?>

<div class="news">
    <div class="news__header">
        <h1 class="news__header-heading">Article headline which runs over three lines</h1>
        <div class="news__header-caption">
            <div class="news__header-by">by</div>
            <div class="news__header-author">USER NAME</div>
            <div class="news__header-date">Oct 5 2015</div>
        </div>
    </div>

    <div class="row">
        <div class="news__slides">
            <div class="slides">
                <div class="news__slide slide" data-thumbnail="images/data/thumbnail-full-article-01.png" style="background-image: url()">
                    <div class="news__slide-caption">Image Caption</div>
                </div>

				 <video controls preload="none" poster="images/data/thumbnail-full-article-01.png">
					<?php require 'partials/cards/_btn-overlay.php'; ?>
				  <source src="images/data/movie.mp4" type="video/mp4">
				  Your browser does not support the video tag.
				</video>

                <div class="news__slide slide" data-thumbnail="images/data/thumbnail-full-article-01.png" style="background-image: url()">
                    <div class="news__slide-caption">Image Caption</div>
                </div>
            </div>
        </div>
        <div class="news__social news__social-topLink">
            <a href="#" class="news__social-link news__social-link--facebook">Share</a>
            <a href="#" class="news__social-link news__social-link--twitter">Share</a>
            <a href="#" class="news__social-link news__social-link--google-plus">Share</a>
			<a href="#" class="news__social-link news__social-link--email">Email</a>
            <a href="#" class="news__social-link news__social-link--comments">23 Comments</a>
        </div>
    </div>

    <div class="row">
        <div class="news__main">
            <div class="news__article">
                <h2 class="news__article-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in pretium ipsum. Ut in sem vitae neque ornare pretium. Consectetur adipiscing elit. </h2>
                <p>Fusce vel justo magna. Etiam orci mi, pharetra eu aliquam sit amet, dapibus vitae sapien. Pellentesque eget sapien felis. Cras eleifend massa at mauris suscipit dictum. Mauris auctor eget leo a fringilla. Aliquam erat volutpat. Nullam vel justo magna rutrum ut eros at lacinia. </p>

                <quote class="news__article-quote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in pretium ipsum. Ut in sem vitae neque ornare pretium. Consectetur adipiscing elit.</quote>

                <div class="news__article-image news__article-image--main" style="background-image:url();">
                    <div class="news__article-image-caption">Image Caption</div>
                </div>

                <p>Fusce vel justo magna. Etiam orci mi, pharetra eu aliquam sit amet, dapibus vitae sapien. Pellentesque eget sapien felis. Cras eleifend massa at mauris suscipit dictum. Mauris auctor eget leo a fringilla. Aliquam erat volutpat. Nullam vel justo magna rutrum ut eros at lacinia. </p>

                <p>Aliquam erat volutpat. Nullam rutrum ut eros at lacinia. Pellentesque elementum est in semper rhoncus. Nunc tristique erat arcu, sit amet fermentum enim mattis ut. Duis luctus tincidunt varius.</p>

                <?php require "partials/cards/_twitter_without_image.php" ?>

                <p>Aliquam erat volutpat. Nullam rutrum ut eros at lacinia. Pellentesque elementum est in semper rhoncus. Nunc tristique erat arcu, sit amet fermentum enim mattis ut. Duis luctus tincidunt varius.</p>

                <div class="news__article-caption">
                    by
                    <div class="news__article-author">Rob Steel</div>
                    <div class="news__article-date">Oct 5 2015</div>
                </div>
            </div>
            <div class="news__social news__social--horizontal">
                <a href="#" class="news__social-link news__social-link--facebook">Share</a>
                <a href="#" class="news__social-link news__social-link--twitter">Share</a>
                <a href="#" class="news__social-link news__social-link--google-plus">Share</a>
                <a href="#" class="news__social-link news__social-link--email">Email</a>
                <a href="#" class="news__social-link news__social-link--comments">23 Comments</a>
            </div>
        </div>
        <div class="news__sidebar">
            <?php require "partials/cards/_news-02.php"; ?>
            <?php require "partials/cards/_twitter_without_image.php"; ?>
            <?php require "partials/cards/_youtube.php"; ?>
            <?php require "partials/cards/_news-02.php"; ?>
            <?php require "partials/cards/_facebook.php"; ?>
            <?php require "partials/cards/_facebook_without_image.php"; ?>
            <?php require "partials/cards/_youtube.php"; ?>
            <?php require "partials/cards/_vimeo.php"; ?>
            <?php require "partials/cards/_vimeo_without_image.php"; ?>
            <?php require "partials/cards/_instagram.php"; ?>
            <?php require "partials/cards/_instagram_without_image.php"; ?>
            <?php require "partials/cards/_vimeo_without_image.php"; ?>
        </div>
    </div>
</div>
<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>
                <div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<?php require "partials/_footer.php"; ?>
