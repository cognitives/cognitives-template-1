<?php require "partials/_header.php"; ?>
<!-- Full style -->
<div class="section">
    <div class="section__content">
        <div class="row">
            <div class="col-full"><?php require "partials/cards/_feature_without_image.php"; ?></div>

            <div class="col-full"><?php require "partials/cards/_news-02.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_news-02_without_image.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_news_ImageOnly.php"; ?></div>

            <div class="col-full"><?php require "partials/cards/_facebook_with_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_facebook_without_image_full.php"; ?></div>

            <div class="col-full"><?php require "partials/cards/_youtube_with_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_youtube_without_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_youtube_ImageOnly.php" ?></div>

            <div class="col-full"><?php require "partials/cards/_twitter_with_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_twitter_without_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_twitter_ImageOnly.php" ?></div>

            <div class="col-full"><?php require "partials/cards/_vimeo_with_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_vimeo_without_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_vimeo_ImageOnly.php"; ?></div>

            <div class="col-full"><?php require "partials/cards/_instagram_with_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_instagram_without_image_full.php"; ?></div>
            <div class="col-full"><?php require "partials/cards/_instagram_ImageOnly.php" ?></div>
        </div>
    </div>
</div>


<div class="section">
    <div class="section__content">
        <div class="row">
            <div class="col-quarter"><?php require "partials/cards/_news-02.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_news-02_without_image.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_news_ImageOnly.php"; ?></div>

            <div class="col-quarter"><?php require "partials/cards/_facebook_with_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_facebook_without_image_full.php"; ?></div>

            <div class="col-quarter"><?php require "partials/cards/_youtube_with_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_youtube_without_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_youtube_ImageOnly.php" ?></div>

            <div class="col-quarter"><?php require "partials/cards/_twitter_with_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_twitter_without_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_twitter_ImageOnly.php" ?></div>

            <div class="col-quarter"><?php require "partials/cards/_vimeo_with_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_vimeo_without_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_vimeo_ImageOnly.php"; ?></div>

            <div class="col-quarter"><?php require "partials/cards/_instagram_with_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_instagram_without_image_full.php"; ?></div>
            <div class="col-quarter"><?php require "partials/cards/_instagram_ImageOnly.php" ?></div>
        </div>
    </div>
</div>


<div class="section">
    
    <div class="container">
        <div class="row" style="margin-top:30px;margin-bottom:30px;">
            <div class="col-full">Card Third Short With Images</div>
        </div>
    </div>
    <div class="section__content">
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_news-02.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_twitter.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_youtube.php"; ?></div>
        </div>
        
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_instagram.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_vimeo.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_facebook.php"; ?></div>
        </div>
    </div>
    
    
    <div class="container">
        <div class="row" style="margin-top:30px;margin-bottom:30px;">
            <div class="col-full">Card Third Short With Images</div>
        </div>
    </div>
    <div class="section__content">
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_news-02_without_image.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_twitter_without_image_full.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_youtube_without_image_full.php"; ?></div>
        </div>
        
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_instagram_without_image_full.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_vimeo_without_image_full.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_facebook_without_image_full.php"; ?></div>
        </div>
    </div>
        
        
    <div class="container">
        <div class="row" style="margin-top:30px;margin-bottom:30px;">
            <div class="col-full">Card Third Short Full Images</div>
        </div>
    </div>
    <div class="section__content">
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_news_ImageOnly.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_twitter_ImageOnly.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_youtube_ImageOnly.php"; ?></div>
        </div>
        
        <div class="row">
            <div class="col-two-thirds"><?php require "partials/cards/_news-01.php" ?></div>
            <div class="col-third-short"><?php require "partials/cards/_instagram_ImageOnly.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_vimeo_ImageOnly.php"; ?></div>
            <div class="col-third-short"><?php require "partials/cards/_youtube_ImageOnly.php"; ?></div>
        </div>
    </div>
</div>


<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<?php require "partials/_footer.php"; ?>
