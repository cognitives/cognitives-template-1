<?php require "partials/_header.php"; ?>
<!-- Full style -->
<div class="section">
    <div class="section__content">
        <div class="row">
            <!--  End Card half section -->
            <div class="col-quarter-short">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter-short">
                <a href="#" class="card card__instagram withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__news withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter-short">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter-short">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-two-thirds">
                <a href="#" class="card video_card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-third-short">
                <a href="#" class="card card__news withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
                <a href="#" class="card card__youtube withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>

            <div class="col-half">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__news ad_icon withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__youtube withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__vimeo withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__instagram withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>

            <!--  End Card half section -->

            <!--  Begin Card one third section -->

            <div class="col-third">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card card__news ad_icon withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card card__youtube withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card card__vimeo withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card card__instagram withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <!--  End Card one third section -->

            <!--  Begin Card one Quarter section -->

            <div class="col-quarter">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter">
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter">
                <a href="#" class="card card__news ad_icon withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter">
                <a href="#" class="card card__youtube withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter">
                <a href="#" class="card card__vimeo withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-quarter">
                <a href="#" class="card card__instagram withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-half">
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <!--  End Card one Quarter section -->

            <!--  Begin Card two third section -->

            <div class="col-two-thirds">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-two-thirds">
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-two-thirds">
                <a href="#" class="card card__news ad_icon withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-two-thirds">
                <a href="#" class="card card__youtube withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-two-thirds">
                <a href="#" class="card card__vimeo withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-two-thirds">
                <a href="#" class="card card__instagram withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>


            <div class="col-full">
                <a href="#" class="card card__twitter withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-full">
                <a href="#" class="card card__facebook withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-full">
                <a href="#" class="card card__news ad_icon withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-full">
                <a href="#" class="card card__youtube withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-full">
                <a href="#" class="card card__vimeo withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <div class="col-full">
                <a href="#" class="card card__instagram withImage__content">
                    <?php require "partials/cards/final/_content.php"; ?>
                </a>
            </div>
            <!--  Begin Card two third section -->

            <!-- Without Image Cards Start -->
        </div>
    </div>
</div>

<?php require "partials/_footer.php"; ?>
