# Newsly Template - HTML & CSS Documentation

## Introduction

Local news was built with the goal of creating a foundational library to aid in all Cognitives based content managed websites.
By creating a set of conventions, it will ideally provide a proven structure which will make subsequent instances quicker and less error prone to build, especially with the necessity to integrate with the back end concepts such as cards. 

The Local news project utilises a few libraries to aid in managing and generating Javascript and CSS, not only for easier development but also optimisation for production use.

These include:

Gulp.js - Gulp is a node based library which allows files to be manipulated in a simple automated fashion. It has the following uses for this project:

* organise all files in a user friendly manner.
* allow for the use of syntactic sugar languages ie. Coffeescript & SASS to author Javscript and CSS.
* compile all files into static, optimized verisons to be integrated into Laravell, or potentially any framework. 
* automate image optimize.
* manage 3rd party libraries such as Bootstrap and jquery plugins 


## Setup

1. Install nodejs
2. Install gulp
3. Configuration
3. Compilation


npm install -g gulp
npm install -g bower
npm install --save-dev coffee-script
npm install
bower install
gulp

## Javascript

Coffeescript is a scripting language which add syntactic sugar to javascript.
JQuery is used as the primary choice of framework 


## Stylesheets

SASS is a scripting language, used to dynamically generate css. Sass's dynamic capabilities such as mixins and variables are used extensively.
BEM is used for class naming convention. BEM stands for Block, Element ,Modifier and is a  convention used for managing complex applications where inheritance and performance become an issue. 

The 2 key concepts within Local News are **columns** and **cards**. Layouts utilises Bootstrap's grids. They are included dynamically via Gulp and SASS, and only the grid libraries of Bootstrap are included. 


##### Columns

- 12 columns 
- 1170px width 
- 0px gutter width

Although the base of the design is a 12 column layout, not all columns are utilized:

Quarters:

- 1/4 - .col-quarter
- 1/2 - .col-half
- 3/4 - .col-three-quarters 

Thirds: 

- 1/3 - .col-third
- 2/3 - .col-two-thirds

(Currently Local news does not make use of half or three quarter width columns).


##### Cards

Cards live inside columns. The column's nesting and the type of card will determine how cards are rendered. In theory, same card can be placed in any layout size and the CSS/JS will render accordingly. (the concept of nesting breaks BEM conventions. However for the sake of the dynamic nature of the app, it is necessary to make the structure user friendly for use. It is the only instance in which nesting is used).

Cards have a base set of styles that are common throughout all sizes and types. To manage this base set, SASS mixins have been used, and have been defined within **card-mixins.scss** and are then 'included' accordingly.

Cards have a default fixed height of 480px. However there is also the addition option to render cards of height 120px (quarter height) and this is managed using the **short** column convention. For example nesting a **.card** within **.col-quarter** will produce a 480px height card, whereas **.col-quarter-short** will produce a 120px height card. Text in all cards have overflow handled by css and javascript. (unfortunately it is not yet technically possible to do so purely in css).

Cards styles are split into categories.



#### Responsive

4 Responsive layout types have been defined:

- Phone
- Tablet
- Desktop
- Large Desktop


##### Phone

Phone is a single column fluid width, auto height. Cards sizes are not distinguishable

##### Tablet

Tablets fluid widths, fixed height and handle columns in a unique manner. Tablets essentially is a 2 column layout, but the width of the column depends on its desktop width.

- 1/2, 2/3 and 3/4 columns become full width
- 1/3 and 1/4 columns become half width

Because of this, adjacent columns need to be considered carefully as this can easily lead to single half width cards on a row, leaving white space.

##### Desktop

Desktop is a fixed width layout of 1170px 

##### Large Desktop

Large Desktop possesses the same behaviour as the Desktop layout with a fixed width of 1600px
 

Responsive styles are inlined within the context of their block, using scss mixins 

.test {
  padding: 40px;

  @include phone {
    padding-left: 10px;
    padding-right: 10px;
  }
}